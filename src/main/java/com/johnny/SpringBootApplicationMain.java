package com.johnny;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@ServletComponentScan//在 springBoot 启动时会扫描@WebServlet，并将该类实例化
@MapperScan("com.johnny.mapper") //@MapperScan 用户扫描MyBatis的Mapper接口
@EnableScheduling //scheduled默认不开启，添加注解后开启定时任务
public class SpringBootApplicationMain {
	public static void main(String[] args) {
		SpringApplication.run(SpringBootApplicationMain.class, args);
	}
}
