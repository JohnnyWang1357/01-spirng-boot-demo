package com.johnny.mapper;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.johnny.pojo.Users;
//@Repository
public interface UsersMapper {
	void insertUser(Users users);
	public List<Users> selectUsersAll();
}
