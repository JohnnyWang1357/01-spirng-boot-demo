package com.johnny.config;

import org.quartz.Trigger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.quartz.CronTriggerFactoryBean;
import org.springframework.scheduling.quartz.MethodInvokingJobDetailFactoryBean;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;


@Configuration
public class QuartzConfigration {

    @Autowired
    private Environment env;
    /**
     * attention: Details：配置定时任务
     */
    @Bean(name = "jobDetail1")
    public MethodInvokingJobDetailFactoryBean detailFactoryBean(ScheduleTask1 task) {// ScheduleTask为需要执行的任务
        MethodInvokingJobDetailFactoryBean jobDetail = new MethodInvokingJobDetailFactoryBean();
        /*
         * 是否并发执行 例如每5s执行一次任务，但是当前任务还没有执行完，就已经过了5s了，
         * 如果此处为true，则下一个任务会执行，如果此处为false，则下一个任务会等待上一个任务执行完后，再开始执行
         */
        jobDetail.setConcurrent(false);

        jobDetail.setName("johnny-joh1");// 设置任务的名字 不能重复
        jobDetail.setGroup("johnny.joh");// 设置任务的分组，这些属性都可以存储在数据库中，在多任务的时候使用

        // 为需要执行的实体类对应的对象
        jobDetail.setTargetObject(task);

        /*
         * execute为需要执行的方法 通过这几个配置，告诉JobDetailFactoryBean我们需要执行定时执行ScheduleTask类中的execute方法
         */
        jobDetail.setTargetMethod("execute");
        return jobDetail;
    }

    /**
     * attention: Details：配置定时任务的触发器，也就是什么时候触发执行定时任务
     */
    @Bean(name = "jobTrigger1")
    public CronTriggerFactoryBean cronJobTrigger(@Qualifier("jobDetail1") MethodInvokingJobDetailFactoryBean jobDetail) {
        CronTriggerFactoryBean tigger = new CronTriggerFactoryBean();
        tigger.setJobDetail(jobDetail.getObject());
		/*
		 * String cron = env.getProperty("batch.cron"); if (cron == null) { cron =
		 * "0/5 * * * * ? *"; }
		 */
        String cron =  "0 0/5 * * * ? *";
        tigger.setCronExpression(cron);// 初始时的cron表达式
        tigger.setName("data-migrate");// trigger的name
        return tigger;
    }

    
    /**
     * attention: Details：配置定时任务--ODS系统数据接入
     */
    @Bean(name = "jobDetail2")
    public MethodInvokingJobDetailFactoryBean detailFactoryBean2(
    		ScheduleTask2 task) {// ScheduleTask为需要执行的任务
        MethodInvokingJobDetailFactoryBean jobDetail = new MethodInvokingJobDetailFactoryBean();
        /*
         * 是否并发执行 例如每5s执行一次任务，但是当前任务还没有执行完，就已经过了5s了，
         * 如果此处为true，则下一个任务会执行，如果此处为false，则下一个任务会等待上一个任务执行完后，再开始执行
         */
        jobDetail.setConcurrent(false);

        jobDetail.setName("johnny-joh2");// 设置任务的名字 不能重复
        jobDetail.setGroup("johnny.joh");// 设置任务的分组，这些属性都可以存储在数据库中，在多任务的时候使用

        // 为需要执行的实体类对应的对象
        jobDetail.setTargetObject(task);

        /*
         * execute为需要执行的方法 通过这几个配置，告诉JobDetailFactoryBean我们需要执行定时执行ScheduleODSTask类中的execute方法
         */
        jobDetail.setTargetMethod("execute");
        return jobDetail;
    }
    /**
     * attention: Details：配置定时任务的触发器，也就是什么时候触发执行定时任务
     */
    @Bean(name = "jobTrigger2")
    public CronTriggerFactoryBean cronJobTrigger1(@Qualifier("jobDetail2") MethodInvokingJobDetailFactoryBean jobDetail) {
        CronTriggerFactoryBean tigger = new CronTriggerFactoryBean();
        tigger.setJobDetail(jobDetail.getObject());
        String cron = 
            cron = "0 0/10 * * * ? *";
        tigger.setCronExpression(cron);// 初始时的cron表达式
        tigger.setName("data-migrate2");// trigger的name
        return tigger;
    }

    
    
    /**
     * attention: Details：配置定时任务--权益人接口数据进入业务表触发时点
     * @param task task
     * @return bean
     */
    @Bean(name = "jobDetail4")
    public MethodInvokingJobDetailFactoryBean detailFactoryBean4(
    		ScheduleTask1 task) {// ScheduleTask为需要执行的任务
        MethodInvokingJobDetailFactoryBean jobDetail = new MethodInvokingJobDetailFactoryBean();
        /*
         * 是否并发执行 例如每5s执行一次任务，但是当前任务还没有执行完，就已经过了5s了，
         * 如果此处为true，则下一个任务会执行，如果此处为false，则下一个任务会等待上一个任务执行完后，再开始执行
         */
        jobDetail.setConcurrent(false);

        jobDetail.setName("johnny-joh4");// 设置任务的名字 不能重复
        jobDetail.setGroup("johnny.joh");// 设置任务的分组，这些属性都可以存储在数据库中，在多任务的时候使用
        // 为需要执行的实体类对应的对象
        jobDetail.setTargetObject(task);

        /*
         * execute为需要执行的方法 通过这几个配置，告诉JobDetailFactoryBean我们需要执行定时执行ScheduleOwnerInterfToReportTask类中的execute方法
         */
        jobDetail.setTargetMethod("execute");
        return jobDetail;
    }

    /**
     * attention: Details：配置定时任务的触发器，也就是什么时候触发执行定时任务
     */
    @Bean(name = "jobTrigger4")
    public CronTriggerFactoryBean cronJobTrigger4(@Qualifier("jobDetail4") MethodInvokingJobDetailFactoryBean jobDetail) {
        CronTriggerFactoryBean tigger = new CronTriggerFactoryBean();
        tigger.setJobDetail(jobDetail.getObject());
        String cron =  "0 0/10 * * * ? *";//0 0/10 * * * ? *
        tigger.setCronExpression(cron);// 初始时的cron表达式
        tigger.setName("data-migrate4");// trigger的name
        return tigger;
    }
    
    /**
     * attention: Details：定义quartz调度工厂
     */
    @Bean(name = "scheduler")
    public SchedulerFactoryBean schedulerFactory(
                                                 @Qualifier("jobTrigger1") Trigger cronJobTrigger1,
                                                 @Qualifier("jobTrigger2") Trigger cronJobTrigger2,
                                                 
                                                 @Qualifier("jobTrigger4") Trigger cronJobTrigger4) {
        SchedulerFactoryBean bean = new SchedulerFactoryBean();
        // 用于quartz集群,QuartzScheduler 启动时更新己存在的Job
        bean.setOverwriteExistingJobs(true);
        // 延时启动，应用启动1秒后
        bean.setStartupDelay(1);
        // 注册触发器
        bean.setTriggers(cronJobTrigger1,cronJobTrigger2,cronJobTrigger4);
        return bean;
    }
}