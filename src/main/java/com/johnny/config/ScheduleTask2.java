package com.johnny.config;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Component;

@Configuration
@Component // 此注解必加
@EnableScheduling // 此注解必加
public class ScheduleTask2{
    /**
     * logger
     */
    private static final Logger logger = LoggerFactory.getLogger(ScheduleTask2.class);
    
    public synchronized void execute() {
    	System.out.println("ScheduleTask222222222 "+new Date());
    }
    
}