package com.johnny.config;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Component;

@Configuration
@Component // 此注解必加
@EnableScheduling // 此注解必加
public class ScheduleTask1{
    /**
     * logger
     */
    private static final Logger logger = LoggerFactory.getLogger(ScheduleTask1.class);
    
    public synchronized void execute() {
    	System.out.println("ScheduleTask11111111 "+new Date());
    }
    
}