package com.johnny.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class MsgController {
	
	@Value("${randomNum}")
	private String randomNum;
	@Value("${helloMsg}")
	private String msg;
	@Value("${hello.name}")
	private String name;
	@RequestMapping("/msg")
	@ResponseBody
	public Map<String,Object> hello(){
		Map<String,Object> map=new HashMap<String,Object>();
		map.put("name","johnny");
		map.put("randomNum", this.randomNum);
		map.put("msg", this.msg);
		map.put("nameyml", this.name);
		return map;
	}
}
