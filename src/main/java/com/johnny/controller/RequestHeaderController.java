package com.johnny.controller;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class RequestHeaderController {
	@RequestMapping("/header")
	@ResponseBody
	public Map<String,Object> hello(HttpServletRequest request){
		Enumeration<String> hnames=request.getHeaderNames();
		String remoteAddr = request.getRemoteAddr();
		Map<String,Object> map=new HashMap<String,Object>();
		map.put("name","johnny");
		System.out.println("hello sss");
		return map;
	}
}
