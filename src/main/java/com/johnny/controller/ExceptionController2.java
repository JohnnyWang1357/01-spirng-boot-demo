package com.johnny.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.johnny.pojo.Users;
import com.johnny.service.UsersService;

@Controller
@RequestMapping("/exception2")
public class ExceptionController2 {
	/**
	 * 页面跳转jsp
	 */
	@RequestMapping("/showError1")
	public String userList(Model model) {
		int i = 0;
		i = 10 / 0;

		return "html/index";
	}

	/**
	 * 页面跳转jsp
	 */
	@RequestMapping("/showError2")
	public String userList2(Model model) {
		List<String> str = null;
		str.size();
		return "html/index";
	}

}
