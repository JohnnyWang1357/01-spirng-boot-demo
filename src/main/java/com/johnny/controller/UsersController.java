package com.johnny.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.johnny.pojo.Users;
import com.johnny.service.UsersService;

@Controller
@RequestMapping("/users")
public class UsersController {
	@Autowired
	private UsersService usersService;

	/**
	 * 页面跳转
	 */
	@RequestMapping("/{page}")
	public String showPage(@PathVariable String page) {
		return page;
	}

	/**
	 * 添加用户
	 */
	@RequestMapping("/addUser")
	public String addUser(Users users) {
		this.usersService.addUser(users);
		return "ok";
	}
	/**
	 * select thymeleafs
	 */
	@RequestMapping("/selectUsersAll")
	public String selectUsersAll(Model model) {
		List<Users> list=this.usersService.selectUsersAll();
		model.addAttribute("list",list);
		return "html/showUsers";
	}
	/**
	 * 页面跳转jsp
	 */
	@RequestMapping("/userList")
	/* @ResponseBody */
	public String showUser(Model model) {
		List<Users> list=this.usersService.selectUsersAll();
		model.addAttribute("list",list);
		return "userList";
	}
}
