package com.johnny.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.johnny.mapper.UsersMapper;
import com.johnny.pojo.Users;
import com.johnny.service.UsersService;
@Service
@Transactional
public class UsersServiceImpl implements UsersService {
	@Autowired
	private UsersMapper usersMapper;
	@Override
	public void addUser(Users users) {
	this.usersMapper.insertUser(users);
	}
	@Override
	public List<Users> selectUsersAll() {
		
		return usersMapper.selectUsersAll();
	}
}
