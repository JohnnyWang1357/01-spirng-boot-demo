package com.johnny.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.johnny.pojo.Users;
@Service
public interface UsersService {
	public void addUser(Users users);
	public List<Users> selectUsersAll();
}
