package com.johnny.test;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.JdkSerializationRedisSerializer;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.johnny.SpringBootApplicationMain;
import com.johnny.pojo.Users;
import com.johnny.service.RedisService;
import com.johnny.service.UsersService;

import redis.clients.jedis.Jedis;
/**
 * SpringBoot测试类
 *@RunWith:启动器 
 *SpringJUnit4ClassRunner.class：让junit与spring环境进行整合
 *
 *@SpringBootTest(classes={App.class}) 1,当前类为springBoot的测试类
 *@SpringBootTest(classes={App.class}) 2,加载SpringBoot启动类。启动springBoot
 *
 *junit与spring整合 @Contextconfiguartion("classpath:applicationContext.xml")
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {SpringBootApplicationMain.class})
public class JunitTest {
	@Autowired
	private UsersService usersService;
	@Autowired
	private RedisTemplate<String, Object> redisTemplate;
	
	@Autowired
	private RedisService redisService;
	 
@Test
public void reids() {
	//存入数据
	 
 redisService.set("key", "data", 10000L);//具体见上面的方法说明
		 
	//读出数据
	Object data= redisService.get("key");
}
	
	//@Test
//	public void selectUser() {
//		List<Users> list=this.usersService.selectUsersAll();
//		System.out.println((list.toString()));
//		System.out.println("----------------------");
//	} 
//	
//	//@Test
//	public void addUser() {
//		Users user=new Users();
//		user.setAge(11);
//		user.setName("junitTest");
//		usersService.addUser(user);
//	}
//	//@Test
//	public void redisAddName() {
//		redisTemplate.opsForValue().set("englishName", "johnnyWang");
//	}
//	//@Test
//	public void redisgetName() {
//		
//		String value=(String)redisTemplate.opsForValue().get("englishName");
//		System.out.println(value);
//	}
//	
//	//@Test
//	public void redisAddUser() {
//		Users user=new Users();
//		user.setAge(11);
//		user.setName("userJohnny");
//		redisTemplate.setValueSerializer(new JdkSerializationRedisSerializer());
//		redisTemplate.opsForValue().set("userJohnny", user);
//	}
//	//redis 直接存储对象比用json存储占用空间大5倍以上
//	//@Test
//	public void redisgetUser() {
//		redisTemplate.setValueSerializer(new JdkSerializationRedisSerializer());
//		Users user=(Users) redisTemplate.opsForValue().get("userJohnny");
//		System.out.println(user);
//	}
//	
	
	 @Test 
 public void redisAddUserJson() { 
 Users user=new Users(); 

 
	  user.setAge(11); // 
	  user.setName("userJohnnyJson"); 
	  redisTemplate.setValueSerializer(new
	  Jackson2JsonRedisSerializer<>(Users.class)); //
	  redisTemplate.opsForValue().set("userJohnnyJson", user);  
	  }
	 //	//redis 直接存储对象比用json存储占用空间大5倍以上
//	//@Test
//	public void redisgetUserJson() {
//		redisTemplate.setValueSerializer(new Jackson2JsonRedisSerializer<>(Users.class));
//		Users user=(Users) redisTemplate.opsForValue().get("userJohnnyJson");
//		System.out.println(user);
//	}
}
